package com.example.retrofitexample.Retrofit;


import retrofit2.Retrofit;

//Class này để chứa đường dẫn và truyền vào RetrofitClient
public class APIUtils {
    public static final String Base_Url = "http://192.168.1.103/webserviceDemo/";

    //func giúp gửi và nhận dữ liệu về, cái mà chứa trong interface DataClient
    public static DataClient getData(){
        return RetrofitClient.getClient(Base_Url).create(DataClient.class);
    }
}
