package com.example.retrofitexample.Retrofit;


import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

//quan ly phuogn thuc day lên server
public interface DataClient {
    //Đối số bên trong post lên tên file code server thực hiện tác vụ nhận ảnh từ cliend là Android App
    @Multipart
    @POST("uploadphoto.php")
    //đối số bên trong Call là kiểu dữ liệu mà server trả về
    Call<String> UploadPhoto(@Part MultipartBody.Part photo);

    @FormUrlEncoded
    @POST("insert.php")
    Call<String> InsertData(@Field("username") String username,
                            @Field("password") String password,
                            @Field("image") String image);

}
