package com.example.retrofitexample.Retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofitClient {
    //tao phuong thuc de khi nguoi dung muon su dung Retrofit thi se goi den phuong thuc nay
    private static Retrofit retrofit = null;
    public static Retrofit getClient(String url){
        OkHttpClient builder = new OkHttpClient.Builder()
                .readTimeout(5000, TimeUnit.MILLISECONDS)
                .writeTimeout(5000,TimeUnit.MILLISECONDS)
                .connectTimeout(10000,TimeUnit.MILLISECONDS)
                .retryOnConnectionFailure(true)
                .build();
        Gson gson = new GsonBuilder().setLenient().create();
        retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(builder)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();//co du lieu json, chuyen json ve gson (bien cua java)
        return retrofit;
    }
}
