package com.example.retrofitexample;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.retrofitexample.Retrofit.APIUtils;
import com.example.retrofitexample.Retrofit.DataClient;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {
    ImageView imageView;
    Button btnRegister;
    EditText username;
    EditText password;
    int REQUEST_CODE_IMAGE = 123;
    String realpath = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        imageView = findViewById(R.id.imageView);
        username = findViewById(R.id.edtUserRegister);
        password = findViewById(R.id.edtPasswordRegister);
        btnRegister = findViewById(R.id.btnRegRegisActivity);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent,REQUEST_CODE_IMAGE);

            }
        });

        //gui len server
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String user = username.getText().toString();
                final String pass = password.getText().toString();
                File file = new File(realpath);
                String file_path = file.getAbsolutePath();
                String[] mangtenfile = file_path.split("\\.");
//                file_path = mangtenfile[0] + System.currentTimeMillis() +'.'+ mangtenfile[1];
                file_path = System.currentTimeMillis() + "." + mangtenfile[1];
                //bắt đầu code gửi photo
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),file);
                MultipartBody.Part body = MultipartBody.Part.createFormData("upload_file",file_path,requestBody);

                DataClient dataClient = APIUtils.getData();
                Call<String> callBack = dataClient.UploadPhoto(body);
                //xu lý sự kiện server trả về

                callBack.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        //do whatever you want here.....
                        //Nếu ảnh gửi thành công thì tiếp tục gửi user và pass lên
                        if(response.body().length()>0){
                            DataClient insertData = APIUtils.getData();
                            Call<String> callBack = insertData.InsertData(user,pass,APIUtils.Base_Url+"image"+response.body());
                            callBack.enqueue(new Callback<String>() {
                                @Override
                                public void onResponse(Call<String> call, Response<String> response) {
                                        Log.d("SUCCESS",response.body());
                                }

                                @Override
                                public void onFailure(Call<String> call, Throwable t) {

                                }
                            });
                        }

                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.d("FAIL",t.getMessage());
                    }
                });
            }
        });
    }


    public String getRealPathFromURI (Uri contentUri) {
        String path = null;
        String[] proj = { MediaStore.MediaColumns.DATA };
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            path = cursor.getString(column_index);
        }
        cursor.close();
        return path;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == REQUEST_CODE_IMAGE && resultCode == RESULT_OK && data!=null){
            Uri uri =data.getData();
            realpath = getRealPathFromURI(uri);
            try {
                InputStream inputStream = getContentResolver().openInputStream(uri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                imageView.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
